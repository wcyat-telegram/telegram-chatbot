import logging
import telegram
import os
import wcyatfiles as files
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

temp = ""
users = []
bottoken = os.environ['bottoken']
bot = telegram.Bot(token=bottoken)
updater = Updater(token=bottoken, use_context=True)
owner = os.environ['owner']
dispatcher = updater.dispatcher

def receive(update, context):
    user = str(update.effective_chat.id)
    if not user == owner:
        try:
            bot.send_message(chat_id=owner, text=user + ": " + update.message.text)
            bot.send_message(chat_id=user, text="Message sent. Please wait for our reply")
        except:
            bot.send_message(chat_id=user, text="Failed sending message. Please notify us at eng.society@engsociety.wcyat.me.")

def reply(update, context, replyto, message):
    bot.send_message(chat_id=replyto, text= "New reply: " + message)

def messageh(update, context):
    if "/reply" in update.message.text and str(update.effective_chat.id) == owner:
        try:
            replyto = ""
            i = 0
            while update.message.text.replace("/reply ", "")[i] != " ":
               replyto += update.message.text.replace("/reply ", "")[i]
               i += 1
            message = update.message.text.replace("/reply " + replyto + " ", "")
            reply(update, context, replyto, message)
        except:
            bot.send_message(chat_id=owner, text="Please check your arguments")
    elif "/ownerbroadcast" in update.message.text:
        ownerbroadcast(update, context)
    else:
        receive(update, context)

def start(update, context):
    global users
    user_id = update.effective_chat.id
    pass_var = False
    if files.checkexist("users"):
        infile = open("users")
        for line in infile:
            if str(user_id) in line:
                pass_var = True
                break
        infile.close()
        if pass_var:
            pass
        else:
            if files.checkexist("users"):
                appendf = "\n" + str(user_id)
            else:
                appendf = str(user_id)
            users.append(str(user_id))
            files.appendfile(filename="users", content=appendf)
    else:
        files.createfile("users")
        appendf = str(user_id) + "\n"
        files.appendfile(filename="users", content=appendf)
    user = str(update.effective_chat.id)
    if user != owner:
        bot.send_message(chat_id=user, text="Type any messages here to contact us.")
    else:
        bot.send_message(chat_id=owner, text="You can receive messages from users through this bot.\nCommands:\n/reply <userid> <message>: send a message to the specified user id.\n/ownerbroadcast <message>: send a message to all users")

def ownerbroadcast(update, context):
    global users
    output = ""
    if str(update.effective_chat.id) != str(owner):
        return
    for i in range(16, len(update.message.text)):
        output += str(update.message.text)[i]
    if files.checkexist("users"):
        users = []
        infile = open("users")
        for line in infile:
            users.append(line)
        for u in users:
            try:
                bot.send_message(chat_id=u, text=output)
            except:
                pass
        return
    return

start_handler = CommandHandler("start", start)
message_handler = MessageHandler(Filters.text, messageh)
dispatcher.add_handler(start_handler)
dispatcher.add_handler(message_handler)
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
updater.start_polling()