# Telegram Chatbot
A telegram bot for anonymous conversations with users. (users ARE NOT anonymous, owner can see their user id)

# Deploy
```
python3 main.py
```

# Commands
Owner
```
/reply (userid) (message): send message to the specified user
/ownerbroadcast (message): send a message to all users
```
Users
```
No commands. Everything they type are forwarded to the owner along with their user id.
```
